package com.kamranali44.javaEightFunction.lambdaExpressions;

public class MyClass {

	public static void main(String[] args) {

		MyClass myClass = new MyClass();
		
		MathOperation multiply = (int a, int b) -> a*b;
		MathOperation add = (a,b) -> a+b;
		MathOperation subtraction = (a,b) -> a-b;
		MathOperation divide = (a,b) -> {return a/b;};
		
		  System.out.println("10 + 5 = " + myClass.operate(10, 5, add));
	      System.out.println("10 - 5 = " + myClass.operate(10, 5, subtraction));
	      System.out.println("10 x 5 = " + myClass.operate(10, 5, multiply));
	      System.out.println("10 / 5 = " + myClass.operate(10, 5, divide));
	      
	      Greeting greeting1 = (str) -> System.out.println("Hello " + str);
	      
	      System.out.println("Greetings: ");
	      greeting1.sayHi("Kamran");
	}
	
	interface Greeting
	{
		void sayHi(String a);
	}
	
	interface MathOperation
	{
		int operate(int a, int b);
	}
	
	public int operate(int a, int b, MathOperation operate)
	{
		return operate.operate(a, b);
	}

}
