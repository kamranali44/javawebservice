package com.kamranali44.javaEightFunction.streams;

public interface Formula {
	double calculate(int a);
	
	default double sqrt(int a)
	{
		return Math.sqrt(a);
	}
}
