package com.kamranali44.javaEightFunction.optional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MyClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
		List<String> filtered = strings.stream().filter(string -> string.equals("Kamran")).collect(Collectors.toList());
		System.out.println(filtered.size());
		
		Integer value1 = null;
		Integer value2 = new Integer(7);

		// Optional.ofNullable - allows passed parameter to be null.
		Optional<Integer> a = Optional.ofNullable(value1);

		// throws NullPointerException if passed parameter is null
		Optional<Integer> b = Optional.of(value2);

		MyClass myClass = new MyClass();

		System.out.println(myClass.Sum(a, b));
	}

	public long Sum(Optional<Integer> a, Optional<Integer> b) {
		System.out.println("First parameter is present: " + a.isPresent());
		System.out.println("Second parameter is present: " + b.isPresent());

		Integer value1 = a.orElse(0);
		Integer value2 = b.get();

		return (value1 + value2);
	}

}
